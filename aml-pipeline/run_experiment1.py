experiment_name = 'Hello_World1_pipeline'

"""
Azure Machine Learning Imports
In this first code cell, we import key Azure Machine Learning modules that we will use below.
"""
import os
import azureml.core
from azureml.core import Workspace, Experiment, Datastore
#from azureml.widgets import RunDetails

# Check core SDK version number
print("SDK version:", azureml.core.VERSION)

"""
Pipeline-specific SDK imports
Here, we import key pipeline modules, whose use will be illustrated in the examples below.
"""

from azureml.pipeline.core import Pipeline
from azureml.pipeline.steps import PythonScriptStep

print("Pipeline SDK-specific imports completed")

"""
Initialize a workspace object from persisted configuration.
"""

ws = Workspace.from_config()
print(ws.name, ws.resource_group, ws.location, ws.subscription_id, sep = '\n')

# Default datastore
def_blob_store = ws.get_default_datastore() 
# The following call GETS the Azure Blob Store associated with your workspace.
# Note that workspaceblobstore is **the name of this store and CANNOT BE CHANGED and must be used as is** 
def_blob_store = Datastore(ws, "workspaceblobstore")
print("Blobstore's name: {}".format(def_blob_store.name))

"""
Upload data to default datastore
Default datastore on workspace is the Azure File storage. 
The workspace has a Blob storage associated with it as well. 
Let's upload a file to each of these storages.
"""

# get_default_datastore() gets the default Azure Blob Store associated with your workspace.
# Here we are reusing the def_blob_store object we obtained earlier
def_blob_store.upload_files(["./20news.pkl"], target_path="20newsgroups", overwrite=True)
print("Upload call completed")

#List of Compute Targets on the workspace
cts = ws.compute_targets
for ct in cts:
    print(ct)
"""
Retrieve or create a Azure Machine Learning compute
"""
from azureml.core.compute import ComputeTarget, AmlCompute
from azureml.core.compute_target import ComputeTargetException

aml_compute_target = "cpu-cluster"
try:
    aml_compute = AmlCompute(ws, aml_compute_target)
    print("found existing compute target.")
except ComputeTargetException:
    print("creating new compute target")
    
    provisioning_config = AmlCompute.provisioning_configuration(vm_size = "STANDARD_D2_V2",
                                                                min_nodes = 1, 
                                                                max_nodes = 4)    
    aml_compute = ComputeTarget.create(ws, aml_compute_target, provisioning_config)
    aml_compute.wait_for_completion(show_output=True, min_node_count=None, timeout_in_minutes=20)
    
print("Azure Machine Learning Compute attached")

# For a more detailed view of current Azure Machine Learning Compute status, use get_status()
# example: un-comment the following line.
# print(aml_compute.get_status().serialize())


"""
Creating a Step in a Pipeline
"""

# Uses default values for PythonScriptStep construct.

source_directory = './train'
print('Source directory for the step is {}.'.format(os.path.realpath(source_directory)))

# Syntax
# PythonScriptStep(
#     script_name, 
#     name=None, 
#     arguments=None, 
#     compute_target=None, 
#     runconfig=None, 
#     inputs=None, 
#     outputs=None, 
#     params=None, 
#     source_directory=None, 
#     allow_reuse=True, 
#     version=None, 
#     hash_paths=None)
# This returns a Step
step1 = PythonScriptStep(name="train_step",
                         script_name="train.py", 
                         compute_target=aml_compute, 
                         source_directory=source_directory,
                         allow_reuse=True)
print("Step1 created")

"""
define few more steps.
"""

# For this step, we use a different source_directory
source_directory = './compare'
print('Source directory for the step is {}.'.format(os.path.realpath(source_directory)))

# All steps use the same Azure Machine Learning compute target as well
step2 = PythonScriptStep(name="compare_step",
                         script_name="compare.py", 
                         compute_target=aml_compute, 
                         source_directory=source_directory)

# Use a RunConfiguration to specify some additional requirements for this step.
from azureml.core.runconfig import RunConfiguration
from azureml.core.conda_dependencies import CondaDependencies
from azureml.core.runconfig import DEFAULT_CPU_IMAGE

# create a new runconfig object
run_config = RunConfiguration()

# enable Docker 
run_config.environment.docker.enabled = True

# set Docker base image to the default CPU-based image
run_config.environment.docker.base_image = DEFAULT_CPU_IMAGE

# use conda_dependencies.yml to create a conda environment in the Docker image for execution
run_config.environment.python.user_managed_dependencies = False

# specify CondaDependencies obj
run_config.environment.python.conda_dependencies = CondaDependencies.create(conda_packages=['scikit-learn'])

# For this step, we use yet another source_directory
source_directory = './extract'
print('Source directory for the step is {}.'.format(os.path.realpath(source_directory)))

step3 = PythonScriptStep(name="extract_step",
                         script_name="extract.py", 
                         compute_target=aml_compute, 
                         source_directory=source_directory,
                         runconfig=run_config)

# list of steps to run
steps = [step1, step2, step3]
print("Step lists created")

# Syntax
# Pipeline(workspace, 
#          steps, 
#          description=None, 
#          default_datastore_name=None, 
#          default_source_directory=None, 
#          resolve_closure=True, 
#          _workflow_provider=None, 
#          _service_endpoint=None)


pipeline1 = Pipeline(workspace=ws, steps=steps)
print ("Pipeline is built")
"""
Validate the pipeline
"""
pipeline1.validate()
print("Pipeline validation complete")


"""
Submit the pipeline
Submitting the pipeline involves creating an Experiment object 
and providing the built pipeline for submission.
"""
# Submit syntax
# submit(experiment_name, 
#        pipeline_params=None, 
#        continue_on_step_failure=False, 
#        regenerate_outputs=False)


pipeline_run1 = Experiment(ws, experiment_name).submit(pipeline1)
print("Pipeline is submitted for execution")

"""
Examine the pipeline run
Use Pipeline SDK objects
"""


run_id = pipeline_run1.name
print("run_id: ".format(run_id))

import time
time.sleep(5)

step_runs = pipeline_run1.get_children()
for step_run in step_runs:
    status = step_run.get_status()
    print('Script:', step_run.name, 'status:', status)
    
    # Change this if you want to see details even if the Step has succeeded.
    if status == "Failed":
        joblog = step_run.get_job_log()
        print('job log:', joblog)

"""
Get additonal run details
If you wait until the pipeline_run is finished, 
you may be able to get additional details on the run. 
Since this is a blocking call, 
the following code is commented out.
"""

#pipeline_run1.wait_for_completion()
#for step_run in pipeline_run1.get_children():
#    print("{}: {}".format(step_run.name, step_run.get_metrics()))